# Jenkins
## Install
- helm repo add jenkins https://charts.jenkins.io
- helm repo update
- helm upgrade --install jenkins jenkins/jenkins -f values.yaml --namespace cicd

# Jenkins Pipeline for Docker Image and Helm Chart Deployment

This Jenkins pipeline automates the process of building a Docker image, creating a Helm chart, and pushing both the Docker image and Helm chart to the respective repositories. The pipeline is designed to work in a Kubernetes environment using Jenkins agents.

## Prerequisites

1. Jenkins with Kubernetes plugin configured.
2. Kubernetes cluster with a 'slave' label.
3. Necessary credentials set up in Jenkins for Docker Hub, GitHub, and Kubernetes.
4. 'build-pod.yaml' file defining the build pod configuration.

## Pipeline Overview

1. **Build Docker Image:**
   - Build a Docker image using the provided Dockerfile.
   - Update the Helm chart values with the Docker image tag and namespace.

2. **Build Helm Chart:**
   - Package the Helm chart with the updated values.
   
3. **Docker & Helm Login and Push:**
   - Log in to Docker Hub using provided credentials.
   - Push the built Docker image to Docker Hub.
   - Push the Helm chart to a specified OCI registry.

4. **Update Helm Values:**
   - Depending on the branch (main, stage, dev):
     - Clone the 'finalproj-argo' repository.
     - Update the Helm chart values with the Docker image tag and namespace based on the branch.
     - Commit and push the changes to the respective branch.

## Usage

1. Configure the Jenkins agent label and Kubernetes pod settings in the `agent` section of the Jenkinsfile.

2. Set up the required environment variables in the `environment` section of the Jenkinsfile. These variables include Docker image details, Docker Hub credentials, GitHub access token, and chart version.

3. Define the stages of the pipeline to execute the desired actions. Customize the steps within each stage if needed.

## Note

This README provides a brief overview of the Jenkins pipeline and its stages. For detailed implementation and usage instructions, refer to the comments within the Jenkinsfile.

For any questions or assistance, please reach out to the pipeline maintainer.

---

Please note that this is a basic overview of the Jenkinsfile, and you might want to include more details or explanations depending on your specific needs and requirements.
